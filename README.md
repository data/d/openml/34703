# OpenML dataset: QSAR-DATASET-FOR-DRUG-TARGET-CHEMBL5578

https://www.openml.org/d/34703

**WARNING: This dataset is archived. Use [meta](https://gitlab.com/data/meta) to discuss it.**

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

This dataset contains QSAR data (from ChEMBL version 17) showing activity values (unit is pseudo-pCI50) of several compounds on drug target ChEMBL_ID: CHEMBL5578 (TID: 101238), and it has 24 rows and 675 features (not including molecule IDs and class feature: molecule_id and pXC50). The features represent Molecular Descriptors which were generated from SMILES strings. Missing value imputation was applied to this dataset (By choosing the Median). Feature selection was also applied.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/34703) of an [OpenML dataset](https://www.openml.org/d/34703). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/34703/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/34703/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/34703/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

